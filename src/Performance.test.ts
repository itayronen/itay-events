import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";
import { Event2 } from "./Event2";

export default function (suite: TestSuite) {

    suite.describe("Performance", () => {
        let event: Event2<number, number>;

        function arrange(test: TestParams) {
            test.arrange();
            event = new Event2<number, number>();
        }

        function runTest(t: TestParams): number {
            let times = 100 * 1000;

            let startTime = new Date().getTime();// performance.now();

            for (let i = 0; i < times; i++) {
                event.trigger(1, 2);
            }

            let elapsed = (new Date().getTime()) - startTime;

            console.log(elapsed);

            return elapsed;
        }

        suite.test("Single listener", (t) => {
            arrange(t);

            t.act();
            event.add((a, b) => { });

            assert.isAtMost(runTest(t), 12);
        });

        suite.test("Two listeners", (t) => {
            arrange(t);

            t.act();
            event.add((a, b) => { });
            event.add((a, b) => { });

            assert.isAtMost(runTest(t), 25);
        });

        suite.test("Many listeners", (t) => {
            arrange(t);

            t.act();
            event.add((a, b) => { });
            event.add((a, b) => { });
            event.add((a, b) => { });
            event.add((a, b) => { });
            event.add((a, b) => { });

            assert.isAtMost(runTest(t), 120);
        });
    });
}