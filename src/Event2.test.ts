import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";
import { Event2 } from "./Event2";

export default function (suite: TestSuite) {
    suite.describe(Event2, (suite) => {
        suite.describe("add", (suite) => {
            suite.test("Simple", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let sum = 0;

                test.act();
                event.add((a, b) => sum += a + b);

                test.assert();
                event.trigger(1, 2);
                event.trigger(3, 4);
                assert.equal(sum, 10);
            });
        });

        suite.describe("add many listeners", (suite) => {
            [2, 3, 4].forEach(count => {
                suite.test(`${count} listeners.`, test => {
                    test.arrange();
                    let event = new Event2<number, number>();
                    let calledCount: number[] = [];

                    for (let i = 0; i < count; i++)
                        calledCount[i] = 0;

                    test.act();
                    for (let i = 0; i < count; i++) {
                        event.add((a, b) => {
                            calledCount[i]++;
                        });
                    }

                    test.assert();
                    event.trigger(1, 2);
                    event.trigger(3, 4);

                    for (let i = 0; i < count; i++)
                        assert.equal(calledCount[i], 2);
                });
            });
        });

        suite.describe("addOnce", (suite) => {
            suite.test("Simple", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let sum = 0;

                test.act();
                event.addOnce((a, b) => sum = a + b);

                test.assert();
                event.trigger(1, 2);
                event.trigger(3, 4);

                assert.equal(sum, 3);
            });
        });

        suite.describe("remove", (suite) => {
            suite.test("After 1 added.", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let isCalled = false;
                let listener = () => isCalled = true;

                event.add(listener);

                test.act();
                let isRemoved = event.remove(listener);

                test.assert();
                event.trigger(1, 2);

                assert.isFalse(isCalled);
                assert.isTrue(isRemoved);
            });

            suite.test("Add 3, then remove first, second and third should be triggered.", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let isCalled1 = false;
                let isCalled2 = false;
                let isCalled3 = false;
                let listener1 = () => isCalled1 = true;
                let listener2 = () => isCalled2 = true;
                let listener3 = () => isCalled3 = true;

                event.add(listener1);
                event.add(listener2);
                event.add(listener3);

                test.act();
                let isRemoved = event.remove(listener1);

                test.assert();
                event.trigger(1, 2);

                assert.isTrue(isRemoved);
                assert.isFalse(isCalled1);
                assert.isTrue(isCalled2);
                assert.isTrue(isCalled3);
            });

            suite.test("Nothing to remove", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let listener = () => { };

                test.act();
                let isRemoved = event.remove(listener);

                test.assert();
                assert.isFalse(isRemoved);
			});
			
			suite.test("After addOnce.", test => {
                test.arrange();
                let event = new Event2<number, number>();
                let isCalled = false;
                let listener = () => isCalled = true;

                event.addOnce(listener);

                test.act();
                let isRemoved = event.remove(listener);

                test.assert();
                event.trigger(1, 2);

                assert.isFalse(isCalled);
                assert.isTrue(isRemoved);
            });
        });

        suite.describe("trigger", (suite) => {
            suite.test("No listeners", test => {
                test.arrange();
                let event = new Event2<number, number>();

                test.act();
                event.trigger(1, 2);
            });

            suite.test("When listener throws, error is ignored and other listeners are called.", test => {
                test.arrange();
                let event = new Event2<number, number>();

                event.add(() => { throw "error"; });

                let isCalled = false;
                event.add(() => isCalled = true);

                test.act();
                event.trigger(1, 2);

                test.assert();
                assert.isTrue(isCalled);
            });

            suite.test("When listener throws an Error object, error is ignored and other listeners are called.", test => {
                test.arrange();
                let event = new Event2<number, number>();

                event.add(() => { throw new Error("some error"); });

                let isCalled = false;
                event.add(() => isCalled = true);

                test.act();
                event.trigger(1, 2);

                test.assert();
                assert.isTrue(isCalled);
            });
        });

        suite.describe(Event2.prototype.hasListeners, (suite) => {
            suite.test("No listeners returns false", test => {
                test.arrange();
                let event = new Event2<number, number>();

                test.act();
                let actual = event.hasListeners();

                test.assert();
                assert.isFalse(actual);
            });

            suite.test("One listener returns true", test => {
                test.arrange();
                let event = new Event2<number, number>();
                event.add(e => { });

                test.act();
                let actual = event.hasListeners();

                test.assert();
                assert.isTrue(actual);
            });

            suite.test("Many listeners returns true", test => {
                test.arrange();
                let event = new Event2<number, number>();
                event.add(e => { });
                event.add(e => { });
                event.add(e => { });
                event.add(e => { });

                test.act();
                let actual = event.hasListeners();

                test.assert();
                assert.isTrue(actual);
            });
        });
    });
}